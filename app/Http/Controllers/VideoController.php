<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class VideoController extends Controller
{
    /**
     * Returns info on uploaded video file
     *
     * @return Response
     */
    public function info(Request $request) {
        // Make sure video file was uploaded
        $this->validate($request, [
            'video' => 'required|mimes:mp4,mkv,flv'
        ]);

        // Save the uploaded video locally
        try {
            $videoFile = $this->saveUploadedVideo($request->file('video'));
        } catch(FileException $e) {
            abort(500, $e->getMessage());
        }

        // Get info
        $info = $this->getInfo($videoFile);

        // Sanitize info
        $info = $this->sanitizeInfo($info);

        // Return JSON response
        return response()->json($info);
    }

    /**
     * Save the uploaded file locally
     *
     * @param Illuminate\Http\UploadedFile $uploadedFile Uploaded file to save
     *
     * @return Symfony\Component\HttpFoundation\File\File Moved file
     */
    private function saveUploadedVideo(UploadedFile $uploadedFile)
    {
        $videosPath = storage_path('videos');
        $originalFileName = $uploadedFile->getClientOriginalName();

        return $uploadedFile->move($videosPath, $originalFileName);    
    }

    /**
     * Get video file info
     *
     * @param Symfony\Component\HttpFoundation\File\File $videoFile File to analyze
     *
     * @return array Video info
     */
    private function getInfo(File $videoFile)
    {
        $getID3 = new \getID3;
        $getID3->setOption(['encoding' => 'UTF-8']);

        $info = $getID3->analyze($videoFile->getPath() . '/' . $videoFile->getFilename());

        return $info;
    }

    /**
     * Sanitize getID3 video info
     *
     * @param array $info Info data to sanitize
     *
     * @return array Clean info data
     */
    private function sanitizeInfo(array $info)
    {
        array_walk_recursive($info, function (&$value, $key) {
            $valid = (bool) preg_match('//u', $value);

            if(!$valid) {
                $value = utf8_encode($value);
            }
        });

        return $info;
    }
}


