<?php

use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class VideoTest extends TestCase
{
    private $tmpMp4File;
    private $tmpFlvFile;
    private $tmpMkvFile;

    /**
     * Set up mock data
     *
     * @return void
     */
    public function setUp()
    {
        $testPath = storage_path('test');

        $origName = 'SampleVideo_1280x720_1mb.mp4';
        $newName = 'tmp_mp4_file.mp4';
        copy($testPath . '/' . $origName, $testPath . '/' . $newName);

        $this->tmpMp4File = new UploadedFile(
            $testPath . '/' . $newName,
            $origName,
            'video/mp4',
            1055736,
            null,
            true
        );

        $origName = 'SampleVideo_1280x720_1mb.flv';
        $newName = 'tmp_flv_file.flv';
        copy($testPath . '/' . $origName, $testPath . '/' . $newName);

        $this->tmpFlvFile = new UploadedFile(
            $testPath . '/' . $newName,
            $origName,
            'video/x-flv',
            1051185,
            null,
            true
        );

        $origName = 'SampleVideo_1280x720_1mb.mkv';
        $newName = 'tmp_mkv_file.mkv';
        copy($testPath . '/' . $origName, $testPath . '/' . $newName);

        $this->tmpMkvFile = new UploadedFile(
            $testPath . '/' . $newName,
            $origName,
            'video/x-matroska',
            1052413,
            null,
            true
        );

        parent::setUp();
    }

    /**
     * Destroy mock data
     *
     * @return void
     */
    public function tearDown()
    {
        if($this->tmpMp4File->isFile()) {
            unlink($this->tmpMp4File->getPath() . '/' . $this->tmpMp4File->getFileName());
        }

        if($this->tmpFlvFile->isFile()) {
            unlink($this->tmpFlvFile->getPath() . '/' . $this->tmpFlvFile->getFileName());
        }

        if($this->tmpMkvFile->isFile()) {
            unlink($this->tmpMkvFile->getPath() . '/' . $this->tmpMkvFile->getFileName());
        }
    }

    /**
     * Test HTTP get on info end point
     *
     * @return void
     */
    public function testInfoGet()
    {
        $response = $this->call('GET', '/api/v1/info');

        $this->assertEquals(405, $response->status());
    }

    /**
     * Test uploading a valid mp4 file
     *
     * @return void
     */
    public function testValidMp4FileUpload()
    {
        $file = 
        $response = $this->call('POST', '/api/v1/info', [], [], ['video' => $this->tmpMp4File]);

        $this->assertEquals(200, $response->status());
        $this->seeJson();
    }

    /**
     * Test uploading a valid flv file
     *
     * @return void
     */
    public function testValidFlvFileUpload()
    {
        $file = 
        $response = $this->call('POST', '/api/v1/info', [], [], ['video' => $this->tmpFlvFile]);

        $this->assertEquals(200, $response->status());
        $this->seeJson();
    }

    /**
     * Test uploading a valid mkv file
     *
     * @return void
     */
    public function testValidMkvFileUpload()
    {
        $file = 
        $response = $this->call('POST', '/api/v1/info', [], [], ['video' => $this->tmpMkvFile]);

        $this->assertEquals(200, $response->status());
        $this->seeJson();
    }
}
